package today;

/**
 * @author Administrator
 */
public class Today {
    public static boolean containsDuplicate(int[] nums) {
        //1.每个整数都遍历一遍
        for(int i=0;i<nums.length-1;i++){
            for(int j=i+1;j<nums.length;j++){
                if(nums[i]==nums[j]){
                    return true;
                }
            }
        }
        return false;
    }
}
